# Config Server #

Config server used to maintain the properties for natwest-sender and natwest-receiver services. Which helps us to do the configuration changes without affecting the microservice code build.
If application enabled with cloud bus configuration in future, microservices don't even requires a restart after the config change.

## Heroku Deployment ##
* https://natwest-config-server.herokuapp.com/



